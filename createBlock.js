'use strict';

const fs = require('fs');
const projectConfig = require('./projectConfig.json');

const dirs = projectConfig.directories;
const mkdirp = require('mkdirp');

const blockName = process.argv[2];
const defaultExtensions = ['scss', 'pug', 'img'];
const extensions = uniqueArray(defaultExtensions.concat(process.argv.slice(3)));

// Use: node createBlock.js [name of block]

if (blockName) {
  const dirPath = `${dirs.source + dirs.blocksDirName}/${blockName}/`;

  mkdirp(dirPath, (err) => {

    if (err) {
      console.error(`Cancel: ${err}`);
    }

    else {
      console.log(`Create directory ${dirPath}`);

      extensions.forEach((extention) => {
        const filePath = `${dirPath + blockName}.${extention}`;
        let fileContent = '';
        let fileCreateMsg = '';

        if (extention === 'scss') {
          fileContent = `.${blockName} {\n  \n}`;
          // fileCreateMsg = '';

          let hasThisBlock = false;
          for (const block in projectConfig.blocks) {
            if (block === blockName) {
              hasThisBlock = true;
              break;
            }
          }
          if (!hasThisBlock) {
            projectConfig.blocks[blockName] = [];
            const newPackageJson = JSON.stringify(projectConfig, '', 2);
            fs.writeFileSync('./projectConfig.json', newPackageJson);
            fileCreateMsg = 'Added in projectConfig.json';
          }
        }

        else if (extention === 'pug') {
          fileContent = ``;
          // fileCreateMsg = '';
        }

        else if (extention === 'js') {
          fileContent = '';
        }

        else if (extention === 'img') {
          const imgFolder = `${dirPath}img/`;
          if (fileExist(imgFolder) === false) {
            mkdirp(imgFolder, (err) => {
              if (err) console.error(err);
              else console.log(`Create directory: ${imgFolder}`);
            });
          } else {
            console.log(`The Directory already added ${imgFolder}`);
          }
        }

        if (fileExist(filePath) === false && extention !== 'img') {
          fs.writeFile(filePath, fileContent, (err) => {
            if (err) {
              return console.log(`The file wasn't created: ${err}`);
            }
            console.log(`File created: ${filePath}`);
            if (fileCreateMsg) {
              console.warn(fileCreateMsg);
            }
          });
        } else if (extention !== 'img') {
          console.log(`The file wasn't created: ${filePath} (already existed)`);
        }

      });
    }
  });
} else {
  console.log('Cancel: no block specified');
}

function uniqueArray(arr) {
  const objectTemp = {};
  for (let i = 0; i < arr.length; i++) {
    const str = arr[i];
    objectTemp[str] = true;
  }
  return Object.keys(objectTemp);
}

function fileExist(path) {
  const fs = require('fs');
  try {
    fs.statSync(path);
  } catch (err) {
    return !(err && err.code === 'ENOENT');
  }
}